using BNG;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class RaycastCollision : MonoBehaviour
{
    LineRenderer lineRenderer;
    public ControllerHand ControllerSide = ControllerHand.Right;
    private VRUISystem vrui;

    void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();
        GenerateCollider();

        vrui = VRUISystem.Instance;
    }

    void Update()
    {
       
    }

    private void GenerateCollider()
    {
        MeshCollider collider = GetComponent<MeshCollider>();

        if (collider == null)
        {
            collider = gameObject.AddComponent<MeshCollider>();
            Mesh mesh = new Mesh();
            lineRenderer.BakeMesh(mesh);
            collider.sharedMesh = mesh;
            collider.convex = true;
            collider.isTrigger = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.layer == LayerMask.NameToLayer("UI"))
        {
            Debug.Log("Hit");
            Debug.Log(ControllerSide);
            vrui.UpdateControllerHand(ControllerSide);
        }
    }
}
