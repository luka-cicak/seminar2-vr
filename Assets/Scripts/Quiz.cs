﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class Quiz : MonoBehaviour
{
    public Button start;
    public Button restart;
    public GameObject quizCanvas;
    public TMP_Text question;
    public Button[] ABCD;
    int index = 0;
    int correctAnswers = 0;
    public TMP_Text result;
    List<List<string>> qaList;
    public TMP_Text counterText;
    bool answered;

    public AudioSource correctSound;
    public AudioSource incorrectSound;
    public AudioSource allIncorrect;
    public AudioSource allCorrect;

    private void Start()
    {
        ABCD[0].onClick.AddListener(delegate { ClickedAnswer("A", 0); });
        ABCD[1].onClick.AddListener(delegate { ClickedAnswer("B", 1); });
        ABCD[2].onClick.AddListener(delegate { ClickedAnswer("C", 2); });
        ABCD[3].onClick.AddListener(delegate { ClickedAnswer("D", 3); });

        qaList = new List<List<string>>
        {
            new List<string> {"Profiler alat nam prikazuje:", "Hijerarhiju poziva prema GPU", "Sve od navedenog", "Izračun jednog okvira", "Ukupno vrijeme svih poziva", "B"},
            new List<string> {"Koliki FPS mora biti da bi perfomanse igre bile zadovoljavajuće?", "30 FPS", "50 FPS", "60 FPS", "90 FPS", "C"},
            new List<string> {"Ako želimo postići FPS koji iznosi 60, koliko mora iznositi vrijeme za izračun jednog okvira?", "33.33 ms", "16.67 ns", "33.33 ns", "16.67 ms", "D"},
            new List<string> {"Što od navedenog NE uzrokuje zagušenje", "Velik broj poligona", "Velik broj vrhova", "Velik broj piksela", "Ništa od navedenog", "D"},
            new List<string> {"Izbaci uljeza.", "Texture Bound", "Lighting", "Pixel Bound", "Memory Bound", "A"},
            new List<string> {"Ako u Profiler alatu vidimo da nam puno vremena odlazi na izračun dinamičkih sjena onda je problem:", "Vertex Bound", "Lighting", "Pixel Bound", "Memory Bound", "A"},
            new List<string> {"Ako prepolovimo rezoluciju i naše performanse se znatno poboljšaju onda je problem:", "Vertex Bound", "Lighting", "Pixel Bound", "Resolution Bound", "C"},
            new List<string> {"Što od navedenog uzrokuje suvišno iscrtavanje quadova (Quad Overdraw)?", "Mali objekti", "Odgovori A i C", "Objekti s puno manjih dijelova", "Sve biljke", "B"},
            new List<string> {"Gdje se čuvaju izračuni često korištenih elemenata kako bi se smanjilo opterećenje na GPU?", "L1 Cache", "SSD", "Priručna memorija od CPU", "Ništa od navedenog", "C"},
            new List<string> {"Ako nam globalno osvjetljenje Lumen usporava performanse, koja kategorija će se pojaviti kao zagušenje u Profiler alatu?", "Lights", "Lighting", "Lumen", "Global Illumination", "C"},
        };
    }

    public void StartQuiz()
    {
        nextQuestion(0);
        quizCanvas.SetActive(true);
        start.gameObject.SetActive(false);
        counterText.gameObject.SetActive(true);
    }

    public void RestartQuiz()
    {
        nextQuestion(0);
        correctAnswers = 0;
        result.text = "";
        result.gameObject.SetActive(false);
        start.gameObject.SetActive(true);
        restart.gameObject.SetActive(false);
    }

    void nextQuestion(int index)
    {
        if (index == qaList.Count)
        {
            result.text = correctAnswers.ToString() + "/" + qaList.Count.ToString() + " točnih dogovora";
            result.gameObject.SetActive(true);

            if (correctAnswers == 0)
                allIncorrect.Play();
            else if (correctAnswers == qaList.Count)
                allCorrect.Play();

            quizCanvas.SetActive(false);
            restart.gameObject.SetActive(true);
            counterText.gameObject.SetActive(false);
            this.index = 0;
            return;
        }

        question.text = qaList[index][0];


        int i = 1;
        foreach (Button btn in ABCD)
        {
            var colors = btn.colors;
            colors.normalColor = Color.white;
            btn.colors = colors;

            btn.GetComponentInChildren<TMP_Text>().text = qaList[index][i];
            i++;
        }

        EnableButtons();

        counterText.text = "10";
        answered = false;
        StartCoroutine(StartCounter());
    }

    void DisableButtons()
    {
        foreach (Button btn in ABCD)
        {
            btn.enabled = false;
        }
    }

    void EnableButtons()
    {
        foreach (Button btn in ABCD)
        {
            btn.enabled = true;
        }
    }


    public void ClickedAnswer(string answer, int indexOfAnswer)
    {
        answered = true;
        EventSystem.current.SetSelectedGameObject(null);
        DisableButtons();

        if (indexOfAnswer == -1)
        {
            incorrectSound.Play();
        }

        else if (answer == qaList[index][5])
        {
            // Correct
            var colors = ABCD[indexOfAnswer].colors;
            colors.normalColor = Color.green;
            ABCD[indexOfAnswer].colors = colors;
            correctAnswers++;

            correctSound.Play();
        }
        else
        {
            // Incorrect
            var colors = ABCD[indexOfAnswer].colors;
            colors.normalColor = Color.red;
            ABCD[indexOfAnswer].colors = colors;

            int correctIndex;
            if (qaList[index][5] == "A")
                correctIndex = 0;
            else if (qaList[index][5] == "B")
                correctIndex = 1;
            else if (qaList[index][5] == "C")
                correctIndex = 2;
            else
                correctIndex = 3;

            var colorsCorr = ABCD[correctIndex].colors;
            colorsCorr.normalColor = Color.green;
            ABCD[correctIndex].colors = colorsCorr;

            incorrectSound.Play();
        }

        StartCoroutine(WaitForNext(4f));
    }

    IEnumerator WaitForNext(float duration)
    {
        float counter = 0;
        while (counter < duration)
        {
            counter += Time.deltaTime;
            yield return null;
        }

        index++;
        nextQuestion(index);
    }

    IEnumerator StartCounter()
    {
        float counter = 10;
        while (counter > 0)
        {
            if (answered) break;

            counter -= Time.deltaTime;
            counterText.text = ((int) counter).ToString();
            yield return null;
        }

        if (answered == false)
            ClickedAnswer("", -1);
    }
}
