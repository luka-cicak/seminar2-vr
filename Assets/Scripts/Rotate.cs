using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Rotate : MonoBehaviour
{
    public GameObject podium;
    public AudioSource move;
    private Vector3 rotation = new Vector3(0, 0, 0);
    bool isRotating = false;
    int index = 0;

    public SimulationQuiz quiz;

    string[] types = { "Quad Overdraw", "Vertex Bound", "Translucency", "Lights" };

    // [ShadowDepthsSUM, TranslucencySUM, BasePassSUM, LightsSUM, ShadowDepths1, ShadowDepths2, ShadowDepths3...]
    string[][] values = {
        new string[] { 
            "1.02 ms", "0.20 ms", "4.34 ms", "0.88 ms",
            "0.51 ms", "0.40 ms", "0.11 ms",
            "0.12 ms", "0.04 ms", "0.04 ms",
            "3.88 ms", "0.24 ms", "0.22 ms",
            "0.53 ms", "0.29 ms", "0.06 ms", 
        },

        new string[] { 
            "4.11 ms", "0.30 ms", "1.06 ms", "1.21 ms",
            "3.05 ms", "0.90 ms", "0.16 ms",
            "0.19 ms", "0.06 ms", "0.05 ms",
            "0.77 ms", "0.18 ms", "0.11 ms",
            "0.90 ms", "0.22 ms", "0.09 ms",
        },

        new string[] { 
            "1.02 ms", "4.99 ms", "1.50 ms", "2.74 ms",
            "0.51 ms", "0.40 ms", "0.11 ms",
            "3.79 ms", "0.69 ms", "0.51 ms",
            "1.11 ms", "0.32 ms", "0.07 ms",
            "0.53 ms", "0.29 ms", "0.06 ms",
        },

        new string[] { 
            "1.76 ms", "0.21 ms", "0.98 ms", "4.92 ms",
            "1.25 ms", "0.40 ms", "0.11 ms",
            "0.18 ms", "0.02 ms", "0.01 ms",
            "0.77 ms", "0.13 ms", "0.08 ms",
            "3.60 ms", "1.02 ms", "0.30 ms",
        },
    };

    float[][] sliders =
    {
        new float[] {1.02f, 0.2f, 4.34f, 0.88f},
        new float[] {4.11f, 0.3f, 1.06f, 1.21f},
        new float[] {1.02f, 4.99f, 1.5f, 2.74f},
        new float[] {1.76f, 0.21f, 0.98f, 4.92f}
    };

    public TMP_Text[] subvalues;

    [Header("Shadows")]
    public Slider sliderShadow;
    public TMP_Text sum1;

    [Header("Translucency")]
    public Slider sliderTranslucency;
    public TMP_Text sum2;

    [Header("BasePass")]
    public Slider sliderBasePass;
    public TMP_Text sum3;

    [Header("Lights")]
    public Slider sliderLights;
    public TMP_Text sum4;

    private void Start()
    {
        SetSliders();
    }

    public void RotateNext()
    {
        if (!isRotating)
        {
            if (index == 3)
                index = 0;
            else
                index++;

            move.Play();
            quiz.NextQuestion();
            StartCoroutine(RotatePodium(3f, false));
        }
    }

    public void RotatePrevious()
    {
        if (!isRotating)
        {
            if (index == 0)
                index = 3;
            else
                index--;

            move.Play();
            quiz.prevQuestion();
            StartCoroutine(RotatePodium(3f, true));
        }
    }

    IEnumerator RotatePodium(float duration, bool isNegative)
    {
        isRotating = true;

        if (isNegative)
            rotation -= new Vector3(0, 90, 0);
        else
            rotation += new Vector3(0, 90, 0);

        Quaternion qRotation = Quaternion.Euler(rotation);
        Quaternion currRotation = podium.transform.rotation;

        float counter = 0;

        while (counter < duration)
        {
            counter += Time.deltaTime;
            podium.transform.rotation = Quaternion.Lerp(currRotation, qRotation, counter / duration);
            yield return null;
        }

        isRotating = false;

        SetSliders();
    }

    void SetSliders()
    {
        EventSystem.current.SetSelectedGameObject(null);

        // Set sliders
        sliderShadow.value = sliders[index][0];
        sliderTranslucency.value = sliders[index][1];
        sliderBasePass.value = sliders[index][2];
        sliderLights.value = sliders[index][3];

        // Set sums
        sum1.text = values[index][0];
        sum2.text = values[index][1];
        sum3.text = values[index][2];
        sum4.text = values[index][3];

        int i = 4;
        foreach (var val in subvalues) {
            val.text = values[index][i];
            i++;
        }
    }
}
