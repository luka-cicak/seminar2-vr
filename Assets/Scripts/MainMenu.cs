using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public void LoadSimulation()
    {
        SceneManager.LoadScene("Simulation");
    }

    public void LoadLections()
    {
        SceneManager.LoadScene("Lections");
    }

    public void LoadQuiz()
    {
        SceneManager.LoadScene("Quiz");
    }

    public void Exit()
    {
        Application.Quit();
    }
}
