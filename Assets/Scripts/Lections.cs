using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;
using UnityEngine.UIElements;

public class Lections : MonoBehaviour
{
    public UnityEngine.UI.Button prev;
    public UnityEngine.UI.Button next;

    [Header("Text files")]
    public TextAsset textFile;
    public TextAsset headersFile;

    [Header("TMP Objects")]
    public TMP_Text header;
    public TMP_Text content;

    public GameObject[] lections;
    private string[] lectionsArray;
    private string[] lectionHeadersArray;

    public ScrollRect scrollView;

    int index = 0;

    private void Start()
    {
        prev.onClick.AddListener(delegate { Prev(); });
        next.onClick.AddListener(delegate { Next(); });

        lectionsArray = textFile.text.Split(";");
        lectionHeadersArray = headersFile.text.Split(";");

        header.text = lectionHeadersArray[index];
        content.text = lectionsArray[index];
        scrollView.normalizedPosition = new Vector2(0, 1);

    }


    void Prev()
    {
        EventSystem.current.SetSelectedGameObject(null);
        index--;
        
        header.text = lectionHeadersArray[index].Remove(0, 2);
        content.text = lectionsArray[index].Remove(0, 2);
        scrollView.normalizedPosition = new Vector2(0, 1);

        if (index == 0)
        {
            prev.gameObject.SetActive(false);
            return;
        }

        if (index < lections.Length - 1)
        {
            next.gameObject.SetActive(true); 
        }
    }

    void Next()
    {
        EventSystem.current.SetSelectedGameObject(null);
        index++;

        header.text = lectionHeadersArray[index].Remove(0, 2);
        content.text = lectionsArray[index].Remove(0, 2);
        scrollView.normalizedPosition = new Vector2(0, 1);

        if (index == lections.Length - 1)
        {
            next.gameObject.SetActive(false);
            return;
        }

        if (index > 0)
        {
            prev.gameObject.SetActive(true);     
        }
    }
}
