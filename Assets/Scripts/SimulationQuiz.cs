using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SimulationQuiz : MonoBehaviour
{
    public Button[] buttons;
    public int[] answers = { 1, 3, 2, 0 };
    int current = 0;

    public AudioSource correctSound;
    public AudioSource incorrectSound;

    private void Start()
    {
        buttons[0].onClick.AddListener(delegate { ClickedAnswer(0); });
        buttons[1].onClick.AddListener(delegate { ClickedAnswer(1); });
        buttons[2].onClick.AddListener(delegate { ClickedAnswer(2); });
        buttons[3].onClick.AddListener(delegate { ClickedAnswer(3); });
    }

    void ClickedAnswer (int answer)
    {
        DisableButtons();

        if (answers[current] == answer)
        {
            correctSound.Play();
            var colors = buttons[answer].colors;
            colors.normalColor = Color.green;
            buttons[answer].colors = colors;
        }
        else
        {
            incorrectSound.Play();
            var colors = buttons[answer].colors;
            colors.normalColor = Color.red;
            buttons[answer].colors = colors;

            var colorsRight = buttons[answers[current]].colors;
            colorsRight.normalColor = Color.green;
            buttons[answers[current]].colors = colorsRight;
        }
    }

     public void NextQuestion ()
    {
        EnableButtons();

        if (current == 3)
        {
            current = 0;
        }
        else
        {
            current++;
        }
    }

    public void prevQuestion ()
    {
        EnableButtons();

        if (current == 0)
        {
            current = 3;
        }
        else
        {
            current--;
        }
    }

    void DisableButtons()
    {
        foreach (Button btn in buttons)
        {
            btn.enabled = false;
        }
    }

    void EnableButtons()
    {
        foreach (Button btn in buttons)
        {
            btn.enabled = true;

            var colors = btn.colors;
            colors.normalColor = Color.white;
            btn.colors = colors;
        }
    }
}
