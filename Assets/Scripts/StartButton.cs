using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using BNG;

public class StartButton : MonoBehaviour
{
    void Update()
    {
       if(InputBridge.Instance.BackButton)
        {
            SceneManager.LoadScene("MainMenu");
        }
    }
}
