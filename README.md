# Vizualizacija tehnika optimizacija u Unreal Engineu

Aplikacija izrađena u sklopu seminara na Fakultetu Elektrotehnike i Računarstva.
Radi se o VR aplikaciji izrađenoj za edukacijske potrebe Tehničke škole Sisak.

## Sadržaj

U repozitoriju se nalazi kompletan Unity projekt sa svim potrebnim Assetima.
Ako namjeravate buildati aplikaciju, morate navigirati do Project Settings -> Player -> Publishing.
Potrebno je napraviti svoj keystore i staviti ga kao zadanog.
